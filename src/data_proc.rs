use itertools::Itertools;
use ndarray::*;
use ndarray_csv::Array2Reader;
use std::fs::File;
use std::io::{Error, ErrorKind};
use std::path::{Path, PathBuf};
use std::result::Result;

#[derive(Debug)]
pub struct NsRun {
    pub logl: ndarray::Array1<f64>,  // LogLikelihood for each sample
    pub thread_labels: Array1<usize>,   // Integral label of thread belonging.
    pub thread_min_max: Array2<f64>, // Each row with index `k`,
    // contains the L from within which the first point in the thread
    // with label `k` was sampled (the "Birth contour") and thee logL
    // of the final point in that thread.
    pub theta: Array2<f64>, // Shape nDims * n_samples
    pub nlive_array: Array1<i32>,
}

fn load_from_txt(path: &Path) -> Result<Array2<f64>, Box<dyn std::error::Error>> {
    let file = File::open(path)?;
    let mut reader = csv::ReaderBuilder::new()
        .has_headers(false)
        .from_reader(file);
    let array = reader.deserialize_array2_dynamic()?;
    Ok(array)
    // TODO ndarray-csv fails to provide the right type.
}

pub fn process_polychord(file_root: &str, base_dir: &str) {
    let dead_birth_file = format!("{}_{}.txt", file_root, "dead_birth");
    let path: PathBuf = [base_dir, &dead_birth_file].iter().collect();
    let samples = load_from_txt(&path);
    // samples = np.loadtxt(os.path.join(base_dir, file_root) + '_dead-birth.txt')
    process_samples_array(&samples.unwrap().view());
    // ns_run = process_samples_array(samples, **kwargs)
    // ns_run['output'] = {'base_dir': base_dir, 'file_root': file_root}
    // if process_stats_file:
    // try:
    // ns_run['output'] = process_polychord_stats(file_root, base_dir)
    // except (OSError, IOError, ValueError, IndexError, NameError,
    // TypeError) as err:
    // warnings.warn(
    // ('process_polychord_stats raised {} processing {}.stats file. '
    // ' I am proceeding without the .stats file.').format(
    // type(err).__name__, os.path.join(base_dir, file_root)),
    // UserWarning)
    // return ns_run
}

fn birth_inds_given_contours(
    birth_logl_arr: &ArrayView1<f64>,
    logl: &ArrayView1<f64>,
) -> Result<Array1<f64>, Box<dyn std::error::Error>> {
    // 	   dup_assert = kwargs.pop('dup_assert', False)
    //     dup_warn = kwargs.pop('dup_warn', False)
    //     if kwargs:
    //         raise TypeError('Unexpected **kwargs: {0}'.format(kwargs))
    //     assert logl_arr.ndim == 1, logl_arr.ndim
    //     assert birth_logl_arr.ndim == 1, birth_logl_arr.ndim
    //     # Check for duplicate logl values (if specified by dup_assert or dup_warn)

    //     nestcheck.ns_run_utils.check_ns_run_logls(
    //         {'logl': logl_arr}, dup_assert=dup_assert, dup_warn=dup_warn)

    //     # Random seed so results are consistent if there are duplicate logls
    //     state = np.random.get_state()  # Save random state before seeding
    //     np.random.seed(0)
    //     # Calculate birth inds
    //     init_birth = birth_logl_arr[0]
    //     assert np.all(birth_logl_arr <= logl_arr), (
    //         logl_arr[birth_logl_arr > logl_arr])
    //     birth_inds = np.full(birth_logl_arr.shape, np.nan)
    //     birth_inds[birth_logl_arr == init_birth] = -1
    //     for i, birth_logl in enumerate(birth_logl_arr):
    //         if not np.isnan(birth_inds[i]):
    //             # birth ind has already been assigned
    //             continue
    //         dup_deaths = np.where(logl_arr == birth_logl)[0]
    //         if dup_deaths.shape == (1,):
    //             # death index is unique
    //             birth_inds[i] = dup_deaths[0]
    //             continue
    //         # The remainder of this loop deals with the case that multiple points
    //         # have the same logl value (=birth_logl). This can occur due to limited
    //         # precision, or for likelihoods with contant regions. In this case we
    //         # randomly assign the duplicates birth steps in a manner
    //         # that provides a valid division into nested sampling runs
    //         dup_births = np.where(birth_logl_arr == birth_logl)[0]
    //         assert dup_deaths.shape[0] > 1, dup_deaths
    //         if np.all(birth_logl_arr[dup_deaths] != birth_logl):
    //             # If no points both are born and die on this contour, we can just
    //             # randomly assign an order
    //             np.random.shuffle(dup_deaths)
    //             inds_to_use = dup_deaths
    //         else:
    //             # If some points are both born and die on the contour, we need to
    //             # take care that the assigned birth inds do not result in some
    //             # points dying before they are born
    //             try:
    //                 inds_to_use = sample_less_than_condition(
    //                     dup_deaths, dup_births)
    //             except ValueError:
    //                 raise ValueError((
    //                     'There is no way to allocate indexes dup_deaths={} such '
    //                     'that each is less than dup_births={}.').format(
    //                         dup_deaths, dup_births))
    //         try:
    //             # Add our selected inds_to_use values to the birth_inds array
    //             # Note that dup_deaths (and hence inds to use) may have more
    //             # members than dup_births, because one of the duplicates may be
    //             # the final point in a thread. We therefore include only the first
    //             # dup_births.shape[0] elements
    //             birth_inds[dup_births] = inds_to_use[:dup_births.shape[0]]
    //         except ValueError:
    //             warnings.warn((
    //                 'for logl={}, the number of points born (indexes='
    //                 '{}) is bigger than the number of points dying '
    //                 '(indexes={}). This indicates a problem with your '
    //                 'nested sampling software - it may be caused by '
    //                 'a bug in PolyChord which was fixed in PolyChord '
    //                 'v1.14, so try upgrading. I will try to give an '
    //                 'approximate allocation of threads but this may '
    //                 'fail.').format(
    //                     birth_logl, dup_births, inds_to_use), UserWarning)
    //             extra_inds = np.random.choice(
    //                 inds_to_use, size=dup_births.shape[0] - inds_to_use.shape[0])
    //             inds_to_use = np.concatenate((inds_to_use, extra_inds))
    //             np.random.shuffle(inds_to_use)
    //             birth_inds[dup_births] = inds_to_use[:dup_births.shape[0]]
    //     assert np.all(~np.isnan(birth_inds)), np.isnan(birth_inds).sum()
    //     np.random.set_state(state)  # Reset random state
    //     return birth_inds.astype(int)
    Result::Err(Box::new(Error::new(ErrorKind::Other, "Oh no.")))
}

fn threads_given_birth_inds(birth_inds: &ArrayView1<f64>) -> Array1<usize> {
    // unique, counts = np.unique(birth_inds, return_counts=True)
    // # First get a list of all the indexes on which threads start and their
    // # counts. This is every point initially sampled from the prior, plus any
    // # indexes where more than one point is sampled.
    // thread_start_inds = np.concatenate((
    //     unique[:1], unique[1:][counts[1:] > 1]))
    // thread_start_counts = np.concatenate((
    //     counts[:1], counts[1:][counts[1:] > 1] - 1))
    // thread_labels = np.full(birth_inds.shape, np.nan)
    // thread_num = 0
    // for nmulti, multi in enumerate(thread_start_inds):
    //     for i, start_ind in enumerate(np.where(birth_inds == multi)[0]):
    //         # unless nmulti=0 the first point born on the contour (i=0) is
    //         # already assigned to a thread
    //         if i != 0 or nmulti == 0:
    //             # check point has not already been assigned
    //             assert np.isnan(thread_labels[start_ind])
    //             thread_labels[start_ind] = thread_num
    //             # find the point which replaced it
    //             next_ind = np.where(birth_inds == start_ind)[0]
    //             while next_ind.shape != (0,):
    //                 # check point has not already been assigned
    //                 assert np.isnan(thread_labels[next_ind[0]])
    //                 thread_labels[next_ind[0]] = thread_num
    //                 # find the point which replaced it
    //                 next_ind = np.where(birth_inds == next_ind[0])[0]
    //             thread_num += 1
    // if not np.all(~np.isnan(thread_labels)):
    //     warnings.warn((
    //         '{} points (out of a total of {}) were not given a thread label! '
    //         'This is likely due to small numerical errors in your nested '
    //         'sampling software while running the calculation or writing the '
    //         'input files. '
    //         'I will try to give an approximate answer by randomly assigning '
    //         'these points to threads.'
    //         '\nIndexes without labels are {}'
    //         '\nIndexes on which threads start are {} with {} threads '
    //         'starting on each.').format(
    //             (np.isnan(thread_labels)).sum(), birth_inds.shape[0],
    //             np.where(np.isnan(thread_labels))[0],
    //             thread_start_inds, thread_start_counts))
    //     inds = np.where(np.isnan(thread_labels))[0]
    //     state = np.random.get_state()  # Save random state before seeding
    //     np.random.seed(0)  # make thread decomposition is reproducible
    //     for ind in inds:
    //         # Get the set of threads with members both before and after ind to
    //         # ensure we don't change nlive_array by extending a thread
    //         labels_to_choose = np.intersect1d(  # N.B. this removes nans too
    //             thread_labels[:ind], thread_labels[ind + 1:])
    //         if labels_to_choose.shape[0] == 0:
    //             # In edge case that there is no intersection, just randomly
    //             # select from non-nan thread labels
    //             labels_to_choose = np.unique(
    //                 thread_labels[~np.isnan(thread_labels)])
    //         thread_labels[ind] = np.random.choice(labels_to_choose)
    //     np.random.set_state(state)  # Reset random state
    // assert np.all(~np.isnan(thread_labels)), (
    //     '{} points still do not have thread labels'.format(
    //         (np.isnan(thread_labels)).sum()))
    // assert np.array_equal(thread_labels, thread_labels.astype(int)), (
    //     'Thread labels should all be ints!')
    // thread_labels = thread_labels.astype(int)
    // # Check unique thread labels are a sequence from 0 to nthreads-1
    // assert np.array_equal(
    //     np.unique(thread_labels),
    //     np.asarray(range(sum(thread_start_counts)))), (
    //         str(np.unique(thread_labels)) + ' is not equal to range('
    //         + str(sum(thread_start_counts)) + ')')
    // return thread_labels
    Array::from(vec![1, 2, 3])
}

fn is_sorted(logl: &ArrayView1<f64>) -> bool {
    // TODO: Usually, the array is already pre-sorted. What we must
    // check is that the array is sorted.
    for i in 0..(logl.len() - 1) {
        if logl[i] >= logl[i + 1] {
            return false;
        }
    }
    true
}



pub fn process_samples_array(
    samples: &ArrayView2<f64>,
) -> Result<NsRun, Box<dyn std::error::Error>> {
    // TODO.
    // samples = samples[np.argsort(samples[:, -2])]

    // TODO
    // ns_run = {}

    // We need an owning representation.
    let logl = samples.slice(s![.., -2]).to_owned();
    let theta = samples.slice(s![.., ..-2]).to_owned();
    let birth_contours = samples.slice(s![.., -1]).to_owned();
    // ns_run['logl'] = samples[:, -2]
    // ns_run['theta'] = samples[:, :-2]
    // birth_contours = samples[:, -1]

    // TODO
    // # birth_contours, ns_run['theta'] = check_logls_unique(
    // #     samples[:, -2], samples[:, -1], samples[:, :-2])

    let birth_inds = birth_inds_given_contours(&birth_contours.view(), &logl.view())?;
    // birth_inds = birth_inds_given_contours(
    //     birth_contours, ns_run['logl'], **kwargs)

    let thread_labels = threads_given_birth_inds(&birth_inds.view());
    // ns_run['thread_labels'] = threads_given_birth_inds(birth_inds)

    let unique_threads = thread_labels.into_iter().collect_vec();
    // unique_threads = np.unique(ns_run['thread_labels'])

    // TODO:
    // assert np.array_equal(unique_threads,
    //                       np.asarray(range(unique_threads.shape[0])))

    // # Work out nlive_array and thread_min_max logls from thread labels and
    // # birth contours
    let mut thread_min_max = Array::zeros((unique_threads.len(), 2));
    // thread_min_max = np.zeros((unique_threads.shape[0], 2))
    // # NB delta_nlive indexes are offset from points' indexes by 1 as we need an
    // # element to represent the initial sampling of live points before any dead
    // # points are created.
    // # I.E. birth on step 1 corresponds to replacing dead point zero

    let delta_nlive: Array1<i32> = Array::zeros(samples.shape()[0] + 1);
    // delta_nlive = np.zeros(samples.shape[0] + 1)

    for label in unique_threads{
		let _inds: Array1<usize> = thread_labels.iter()
			.enumerate()
			.filter(|(ind, lab)| {return **lab == *label})
			.map(|(ind, _)| {ind})
			.collect();
		thread_min_max[(*label,1)] = logl[_inds[_inds.len()-1]];
	}
    // for label in unique_threads:
    //     thread_inds = np.where(ns_run['thread_labels'] == label)[0]
    //     # Max is final logl in thread
    //     thread_min_max[label, 1] = ns_run['logl'][thread_inds[-1]]
    //     thread_start_birth_ind = birth_inds[thread_inds[0]]
    //     # delta nlive indexes are +1 from logl indexes to allow for initial
    //     # nlive (before first dead point)
    //     delta_nlive[thread_inds[-1] + 1] -= 1
    //     if thread_start_birth_ind == birth_inds[0]:
    //         # thread minimum is -inf as it starts by sampling from whole prior
    //         thread_min_max[label, 0] = -np.inf
    //         delta_nlive[0] += 1
    //     else:
    //         assert thread_start_birth_ind >= 0
    //         thread_min_max[label, 0] = ns_run['logl'][thread_start_birth_ind]
    //         delta_nlive[thread_start_birth_ind + 1] += 1

    // TODO:
    // ns_run['thread_min_max'] = thread_min_max

    let nlive_array = delta_nlive
        .iter()
        .scan(0, |state, &x| {
            *state += x;
            Some(*state)
        })
        .collect::<Array1<i32>>()
        .slice(s![..-1])
        .to_owned();
    // ns_run['nlive_array'] = np.cumsum(delta_nlive)[:-1]

    // TODO:
    // return ns_run
    Result::Ok(NsRun {
        logl,
        thread_labels,
        thread_min_max,
        theta,
        nlive_array,
    })
}
